import ReactDOM from 'react-dom/client'
import { BrowserRouter } from "react-router-dom";
import { QueryClient, QueryClientProvider } from 'react-query'

import Routers from './routers/Routers'

import './styles/init.scss'
import 'antd/dist/reset.css';

const queryClient = new QueryClient()

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <BrowserRouter>
    <QueryClientProvider client={queryClient}>
      <Routers />
    </QueryClientProvider>
  </BrowserRouter>
)
