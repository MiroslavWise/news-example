import { FC, ReactNode, useEffect } from "react";
import { Routes, Route } from "react-router-dom";

import Main from "src/pages/Main";
import Current from "src/pages/Current";

import { useListNews } from "src/store/news";

interface IRoute {
        path: string
        element: ReactNode
}

const PAGES: IRoute[] = [
        {
                path: '/',
                element: <Main />,
        },
        {
                path: '/news/:id',
                element: <Current />
        },
]

const Routers: FC = () => {

        const setLoading = useListNews(state => state.setLoading)
        const getData = useListNews(state => state.setData)

        useEffect(() => {
                setLoading(true)
                getData()
                
                const interval = setInterval(() => {
                        getData()
                }, 60 * 1000)

                return () => clearInterval(interval)
        }, [])

        return (
                <div className="wrapper">
                        <div className="container">
                                <Routes>
                                        {
                                                PAGES.map(({ path, element }) => (
                                                        <Route
                                                                key={path}
                                                                element={element}
                                                                path={`${path}_route`}
                                                        />
                                                ))
                                        }
                                        <Route path="*" element={<Main />} />
                                </Routes>
                        </div>
                </div>
        )
}

export default Routers