

import type { IData } from "src/api/api";

export interface IUseNewsList {
        data: IData[]
        loading: boolean
        
        setData(): void
        setLoading(value: boolean): void
}

export interface IUseComment{
        data: {
                [key: string | number]: IData
        }

        setData(value: IData): void
}