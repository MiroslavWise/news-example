import { create } from "zustand";
import axios from "axios";

import type { IUseNewsList, IUseComment } from "./types/type-news";

import { newsList, IData, currentNews, URL_ } from "src/api/api";

export const useListNews = create<IUseNewsList>(
        (set, get) => ({
                data: [],
                loading: false,

                setData() {
                        axios(`${URL_}/newstories.json`)
                                .then((response: { data: number[] }) => {
                                        Promise.all([
                                                ...response.data
                                                        .slice(0, 100)
                                                        .map((num: number) => get().data.find(item => item.id === num) || currentNews(num))
                                        ])
                                                .then((value: IData[]) => { set({ data: value }) })
                                                .finally(() => {
                                                        if (get().loading) set({ loading: false })
                                                })
                                })
                                .catch(e => { if (get().loading) set({ loading: false }) })
                },
                setLoading(value) {
                        set({ loading: value })
                }
        })
)

export const useComment = create<IUseComment>(
        (set, get) => ({
                data: {},

                setData: (value) => {
                        if (!Object.hasOwn(get().data, value?.id)) {
                                set({
                                        data: {
                                                ...get().data,
                                                [value?.id]: value
                                        }
                                })
                        }
                }
        })
)