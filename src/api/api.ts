export const URL_ = 'https://hacker-news.firebaseio.com/v0'
import axios from "axios"

export const newsList = (): Promise<IData[]> => {
        return axios(`${URL_}/newstories.json`)
                .then((response: { data: number[] }) => {
                        return Promise.all([
                                ...response.data
                                        .slice(0, 99)
                                        .map(item => currentNews(item))
                        ])
                })
                .catch(e => {
                        console.error('ERROR LIST NEWS: ', e)

                        return e
                })
}

type TType = "job" | "story" | "comment" | "poll" | "pollopt"

export interface IData{
        by : string
        descendants : number
        id : number
        kids : number[]
        score : number
        time : number
        title : string
        type : TType
        url: string
        text: string
}

export const currentNews = (id: any): Promise<IData> => {
        console.log(`${URL_}/item/${id}.json`)

        return axios(`${URL_}/item/${id}.json`)
                .then(response => response.data)
                .catch(e => {})
}