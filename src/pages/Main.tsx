import { FC, useMemo } from "react";
import { useNavigate } from "react-router";
import moment from "moment";

import { Spin, Progress, Typography, Button } from "antd";

import { useListNews } from "src/store/news";

const { Text, Title } = Typography

const Main: FC = () => {
        const navigate = useNavigate()

        const data = useListNews(state => state.data)
        const loading = useListNews(state => state.loading)
        const getData = useListNews(state => state.setData)

        // const maxRate: number = useMemo(() => {
        //         let max = -Infinity
        //         data.forEach(item => {
        //                 if(item?.score > max) max = item?.score
        //         })
        //         return max
        // }, [data])

        return (
                <Spin spinning={loading} style={{minHeight: '100vh'}} size="large">
                        <div className="container-news">
                                <div className="title-container">
                                        <Title level={3}>News</Title>
                                        <div onClick={getData} className="button-reload-news">
                                                Reload news
                                        </div>
                                </div>
                                {
                                        data?.map(item => (
                                                <div key={`${item?.id}_new`} onClick={() => navigate(`/news/${item?.id}`)} className="item-news">
                                                        <div className="title-container">
                                                                <p className="title">
                                                                        {item?.title}
                                                                </p>
                                                                <Text className="time" italic type="secondary">
                                                                        {moment.unix(item?.time).format('LLL')}
                                                                </Text>
                                                        </div>
                                                        <div className="title-container">
                                                                <Text strong>
                                                                        Rate: <Text  italic>{item?.score}</Text>
                                                                </Text>
                                                                <Text>
                                                                        {item?.by}
                                                                </Text>
                                                        </div>
                                                </div>))
                                }
                        </div>
                </Spin>
        )
}

export default Main