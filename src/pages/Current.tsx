import { FC, useEffect, useRef } from "react";
import { useNavigate, useParams } from "react-router";
import { useQuery } from "react-query";
import moment from "moment";

import { Button, Divider, Spin, Typography } from "antd";

import Comment from "src/components/Comment";

import { currentNews, IData } from "src/api/api";

import { useComment } from "src/store/news";

const { Text, Title } = Typography

const Current: FC = () => {
        const navigate = useNavigate()
        const { id_route } = useParams()
        const text = useRef<HTMLDivElement>(null)
        const { data, isLoading, refetch } = useQuery(['news/', id_route], () => currentNews(id_route))

        const setComment = useComment(state => state.setData)

        useEffect(() => {
                if (text.current && data?.text) {
                        text.current.innerText = data?.text
                }
        }, [data?.text])

        const handleReloadComments = () => {
                refetch()
                        .then(data => {
                                if (data?.data && data?.data?.kids?.length > 0) {
                                        Promise.all([
                                                ...data?.data?.kids.map(num => currentNews(num))
                                        ])
                                                .then(data => {
                                                        data.forEach(item => { setComment(item) })
                                                })
                                }
                        })
        }

        return (
                <Spin spinning={isLoading} style={{minHeight: '100vh', width: '100%'}} size="large">
                        <div className="current-container">
                                <div className="button-back" onClick={() => navigate('/')}>
                                        <p>Back to the list</p></div>
                                <div className="title-container">
                                        <Title level={3}>{data?.title}</Title>
                                        {
                                                data?.time
                                                &&
                                                <Text italic type="secondary">{ moment.unix(data?.time).format('LLL') }</Text>
                                        }
                                </div>
                                <div style={{marginTop: '-20px'}}><Text>{ data?.by }</Text></div>
                                <div className="text">
                                        <Text  ref={text} />
                                </div>
                                {
                                        data?.url
                                        &&
                                        <div className="link">
                                                <Text><a href={data?.url} target="_blank">{ data?.url }</a></Text>
                                        </div>
                                }
                                <div style={{width: 'calc(100% + 60px)', marginLeft: '-30px', height: 5, backgroundColor: 'white'}} />
                                <div className="comments">
                                        <div className="comments-title">
                                                <Text type="secondary">Comments on the news</Text>
                                                <Button type="link" onClick={handleReloadComments}>Reload comments</Button>
                                        </div>
                                        {
                                                data && data?.kids?.length > 0
                                                ?
                                                        <>
                                                                <Divider />
                                                                <Comment
                                                                        data={data}
                                                                />
                                                        </>
                                                        :
                                                        <>
                                                                <Text italic>There are no comments on this news yet</Text>
                                                        </>

                                        }

                                </div>
                        </div>
                </Spin>
        )
}

export default Current