import React, { FC, useEffect } from "react";
import moment from "moment";

import { Button, Divider, Typography } from "antd";

import { currentNews, IData } from "src/api/api";

import { useComment } from "src/store/news";

const recursionComment = (data: IData, setComment: (args: IData) => void) => {
        if (data && data?.kids?.length > 0) {
                Promise.all([
                        ...data?.kids?.map(item => currentNews(item))
                ])
                        .then(data => {
                                data?.forEach(item => {
                                        setComment(item)
                                })
                        })
        }
}

const { Text } = Typography

interface IProps{
        data: IData
}

const Comment: FC<IProps> = ({ data }) => {
        const comment = useComment(state => state.data)
        const setComment = useComment(state => state.setData)
        
        useEffect(() => {
                if (data && data?.kids?.length > 0) {
                        recursionComment(data, setComment)
                }
        }, [data?.kids])

        return (
                <div className="" key={`${data?.id}_key_comment`}>
                        {
                                data && data?.kids?.every(item => Object.hasOwn(comment, item))
                                        &&
                                        data?.kids?.map((num, index) => (
                                                <React.Fragment key={`${num}_key_sub`}>
                                                        <div  className="item-comment">
                                                                <div className="title-author-time">
                                                                        <Text>
                                                                                {comment[num]?.by}
                                                                        </Text>
                                                                        <Text italic type="secondary">
                                                                                {comment[num]?.time && moment.unix(comment[num]?.time).format('LLL')}
                                                                        </Text>
                                                                </div>
                                                                <div className="description">
                                                                        <Text type="secondary">
                                                                                {comment[num]?.text}
                                                                        </Text>
                                                                        {
                                                                                comment[num]?.kids?.length > 0
                                                                                &&
                                                                                <div className="button-more">
                                                                                        <p
                                                                                                onClick={() => {
                                                                                                        recursionComment(comment[num], setComment)
                                                                                                }}
                                                                                        >More comment</p>
                                                                                </div>
                                                                        }
                                                                </div>
                                                                {
                                                                        comment[num]?.kids?.length > 0 && comment[num]?.kids?.every(item => Object.hasOwn(comment, item))
                                                                        &&
                                                                        <Comment
                                                                                data={comment[num]}
                                                                        />
                                                                }
                                                        </div>
                                                        {
                                                                index !== data?.kids?.length - 1
                                                                &&
                                                                <Divider />
                                                        }
                                                </React.Fragment>
                                        ))
                        }
                </div>
        )
}

export default Comment